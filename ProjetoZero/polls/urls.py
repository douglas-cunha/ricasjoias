from django.urls import path
from . import views

urlpatterns = [
    path('cadastrar/', views.Cadastrar.as_view(), name='cadastrar'),
    path('', views.exibirProdutos, name='exibirProdutos'),
    path('meuCarrinho', views.exibirCarrinho, name="meuCarrinho"),
]