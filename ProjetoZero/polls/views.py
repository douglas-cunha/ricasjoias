from django.contrib.auth.decorators import login_required
from django.db.models import QuerySet
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from polls.models import Produto, Categoria, Imagem, Usuario, Carrinho, Carrinho_Produto
from .forms import CriarUsuarioForm

class Cadastrar(generic.CreateView):
    form_class = CriarUsuarioForm
    success_url = reverse_lazy('login')
    template_name = 'polls/cadastrar.html'


def exibirProdutos(request):
    return render(request, 'polls/home.html', {'produto': Produto.objects.all(), 'categoria': Categoria.objects.all(), 'imagem': Imagem.objects.all()})

def exibirPorCategoria(request, id):
    return render(request, 'polls/categoria.html', {'produto': Produto.objects.filter(categoria_id=id), 'categoria': Categoria.objects.all()})

def exibirDetalhesProduto(request, id):
    return render(request, 'polls/detalhesProduto.html', {'produto': Produto.objects.filter(id=id)})

@login_required
def adicionarAoCarrinho(request, idProduto):
    produto = Produto.objects.get(id=idProduto)
    usuario = get_perfil_logado(request)
    carrinho = Carrinho.objects.get(usuario_id=str(usuario.id))
    produto.adicionarAoCarrinho(carrinho)
    return exibirDetalhesProduto(request, produto.id)

@login_required
def get_perfil_logado(request):
    return request.user

@login_required
def exibirCarrinho(request):
    valorTotal = 0
    usuario = get_perfil_logado(request)
    carrinho = Carrinho.objects.get(usuario_id=str(usuario.id))
    produtos = Carrinho_Produto.objects.filter(carrinho_id=str(carrinho.id))
    for p in produtos:
        valorTotal += p.produto.preco
    return render(request, 'polls/meuCarrinho.html', {'produtos': produtos, 'valorTotal': valorTotal})

def removerDoCarrinho(request, idProduto):
    usuario = get_perfil_logado(request)
    carrinho = Carrinho.objects.get(usuario_id=str(usuario.id))
    Carrinho_Produto.objects.get(produto_id=idProduto, carrinho_id=str(carrinho.id)).delete()
    return exibirCarrinho(request)

def pagarCompra(request, idProduto):
   produto = Produto.objects.get(id=idProduto)
   produto.qt_estoque = produto.qt_estoque-1
   produto.save()
   return removerDoCarrinho(request, idProduto)

