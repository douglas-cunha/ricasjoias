from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import Usuario

class CriarUsuarioForm(UserCreationForm):

    class Meta:
        model = Usuario
        fields = ('first_name', 'last_name', 'username', 'email', 'cpf', 'sexo', 'endereco', )

class EditarUsuarioForm(UserChangeForm):

    class Meta:
        model = Usuario
        fields = ('first_name', 'last_name', 'username', 'email', 'cpf', 'sexo', 'endereco', )