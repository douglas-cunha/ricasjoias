# -*- coding:utf-8 -*-
from django.contrib.auth.models import AbstractUser
from django.db import models
import datetime
from django.utils import timezone


# Create your models here.

class Categoria(models.Model):
    categoria = models.CharField(max_length=50)

    def __str__(self):
        return self.categoria


class Produto(models.Model):
    id = models.AutoField(primary_key=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, default=0)
    nome = models.CharField(max_length=40)
    preco = models.DecimalField(max_digits=10, decimal_places=2)
    qt_estoque = models.IntegerField(default=0)
    descricao = models.TextField()

    def __str__(self):
        return self.nome

    def adicionarAoCarrinho(self, carrinho):
        Carrinho_Produto(produto=self, carrinho=carrinho).save()

class Imagem(models.Model):
    class Meta:
        ordering = ('produto', 'nome')

    produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
    nome = models.CharField(max_length=100)

    original = models.ImageField(
        null=True,
        blank=True,
        upload_to='media/galeria/original',
    )
    thumbnail = models.ImageField(
        null=True,
        blank=True,
        upload_to='media/galeria/thumbnail',
    )

class Usuario(AbstractUser):
    cpf = models.CharField(max_length=11)
    sexo = models.CharField(max_length=1)
    endereco = models.CharField(max_length=130)

    def alterar_endereco(self, novoEndereco):
        self.endereco = novoEndereco


class Carrinho(models.Model):
    id = models.AutoField(primary_key=True)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, default=0)

class Carrinho_Produto(models.Model):
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE, default=0)
    carrinho = models.ForeignKey(Carrinho, on_delete=models.CASCADE, default=0)