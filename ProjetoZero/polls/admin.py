
# Register your models here.
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from polls.forms import EditarUsuarioForm, CriarUsuarioForm
from .models import Categoria, Usuario, Produto, Imagem, Carrinho, Carrinho_Produto

admin.site.register(Categoria)
admin.site.register(Produto)

class UsuarioAdmin(UserAdmin):
    add_form = CriarUsuarioForm
    form = EditarUsuarioForm
    model = Usuario
    list_display = ['email', 'username','first_name', 'last_name', 'cpf', 'sexo', 'endereco',]

admin.site.register(Usuario, UsuarioAdmin)

class AdminImagem(ModelAdmin):
    list_display = ('produto','nome',)
    list_filter = ('produto',)
    search_fields = ('nome',)

admin.site.register(Imagem, AdminImagem)
admin.site.register(Carrinho)
admin.site.register(Carrinho_Produto)
