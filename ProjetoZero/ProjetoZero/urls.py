"""ProjetoZero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.templatetags.static import static
from django.urls import include, path
from django.views.generic.base import TemplateView

from ProjetoZero import settings
from polls import views

urlpatterns = [
    path('', views.exibirProdutos, name='home'),
    path('categoria/(?C<id>\d+)', views.exibirPorCategoria, name="exibirPorCategoria"),
    path('categoria/produto(?C<id>\d+)', views.exibirDetalhesProduto, name="exibirDetalhesProduto"),
    path('admin/', admin.site.urls),
    path('polls/', include('polls.urls')),
    path('polls/', include('django.contrib.auth.urls')),
    path('adicionarProduto/(?C<idProduto>\d+)', views.adicionarAoCarrinho, name="adicionarAoCarrinho"),
    path('removerProduto/(?C<idProduto>\d+)', views.removerDoCarrinho, name="removerDoCarrinho"),
    path('pagarCompra/(?C<idProduto>\d+)', views.pagarCompra, name="pagarCompra"),

    #static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),
]
